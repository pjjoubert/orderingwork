﻿using System;
using System.Threading.Tasks;

namespace OrderingWork.Core.Interfaces
{
    public interface IStepOneService
    {
        Task<string> DoStepOneCalculation(Guid input);
    }
}
