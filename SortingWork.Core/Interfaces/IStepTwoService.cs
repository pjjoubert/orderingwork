﻿using System;
using System.Threading.Tasks;

namespace OrderingWork.Core.Interfaces
{
    public interface IStepTwoService
    {
        Task<string> DoStepTwoCalculation(Guid input);
    }
}
