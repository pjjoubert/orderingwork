﻿using OrderingWork.Core.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OrderingWork.Core.Services
{
    public class StepTwoService : IStepTwoService
    {
        public StepTwoService()
        {

        }

        public async Task<string> DoStepTwoCalculation(Guid input)
        {
            long numbers = 0;
            await Task.Run(() => { 
                var guidString = input.ToString();
                numbers = (from g in guidString where char.IsDigit(g) select g).ToArray().Select(c => Convert.ToInt64(c.ToString())).Sum();
            });
            return numbers.ToString();
        }
    }
}
