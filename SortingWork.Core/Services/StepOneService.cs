﻿using OrderingWork.Core.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OrderingWork.Core.Services
{
    public class StepOneService : IStepOneService
    {
        public StepOneService()
        {

        }

        public async Task<string> DoStepOneCalculation(Guid input)
        {
            var guidString = input.ToString();
            string result = "";
            await Task.Run(() => {
                result = new string((from s in guidString where s != '-' orderby s select s).ToArray()).ToString();
            });

            return result;
        }
    }
}
