﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OrderingWork.Core;
using OrderingWork.Core.Interfaces;
using OrderingWork.Data;

namespace OrderingWork.Pages
{
    public class IndexModel : PageModel
    {       
        private readonly IStepOneService _stepOneService;
        private readonly IStepTwoService _stepTwoService;

        public IEnumerable<Work> Data { get; set; }
        public List<TimeSpan> StepOneTimeTaken = new List<TimeSpan>();
        public List<TimeSpan> StepTwoTimeTaken = new List<TimeSpan>();
        public TimeSpan TotalTime = TimeSpan.Zero;


        private readonly IWorkData _workData; //datasource

        public IndexModel(IStepOneService stepOneService,
                            IStepTwoService stepTwoService,
                            IWorkData workData)
        {            
            _workData = workData;
            _stepOneService = stepOneService;
            _stepTwoService = stepTwoService;
        }

        public async void OnGet()
        {
            Data = _workData.GetWork();
            var timer = new Stopwatch();

            foreach (var item in Data)
            {
                timer.Start();
                item.Step1Result = await _stepOneService.DoStepOneCalculation(item.Id);
                timer.Stop();
                StepOneTimeTaken.Add(timer.Elapsed);
                TotalTime = TotalTime + timer.Elapsed;
                timer.Reset();

                timer.Start();
                item.Step2Result = await _stepTwoService.DoStepTwoCalculation(item.Id);
                timer.Stop();
                StepTwoTimeTaken.Add(timer.Elapsed);
                TotalTime = TotalTime + timer.Elapsed;
                timer.Reset();

            }
        }
    }
}
