﻿using OrderingWork.Core;
using System.Collections.Generic;

namespace OrderingWork.Data
{
    public interface IWorkData
    {
        IEnumerable<Work> GetWork();
    }
    
}
