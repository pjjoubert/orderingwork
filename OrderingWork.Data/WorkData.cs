﻿using System;
using System.Collections.Generic;
using OrderingWork.Core;
using System.Linq;

namespace OrderingWork.Data
{
    //Dummy datasource 
    public class WorkData : IWorkData
    {
        readonly List<Work> worklist;

        public WorkData()
        {
            worklist = new List<Work>();

            for (var i = 0; i <= 10000; i++)
            {
                worklist.Add(new Work());
            }
        }

        public IEnumerable<Work> GetWork() //Only need a get for the time being
        {
            return from w in worklist select w;
        }
    }
}
